package validator
{
	
	import spark.components.TextArea;
	
	public class ContentReviewer 
	{
		public var _TextTarget:TextArea;	
		private var i:int;
		
		public function ContentReviewer(_textTarget:TextArea)
		{
			_TextTarget = _textTarget;	
			
		}
		
		public function process(_xml:XML):void{
			var personIDList:Vector.<String> = new Vector.<String>();
			var questionIDList:Vector.<String> = new Vector.<String>();
			var str:String = "";
			var substr:String ="";
			var _error:Boolean;
			var _errorCount:int;
			if(_xml.localName() == "CaseStudy"){
				str = "\"CaseStudy\" is the root element: " + (_xml.localName() == "CaseStudy")+".\n";
			} else {
				str = "ERROR: root element is not CaseStudy.\n";
			}
			str += "\nPlayerAvatar role: " + _xml.PlayerAvatar.@Role + 
				" organization: " + _xml.PlayerAvatar.@Organization + "\n";
			str +=  "\n" +_xml.People.Person.length() + " people in the case study.\n" ;
			str += "\n";

			for (i=0;i<_xml.People.Person.length();i++){
				personIDList.push(_xml.People.Person[i].@id);
			}
			
			for(i=0;i<_xml..Pairing.length();i++){
				if(personIDList.indexOf(_xml..Pairing[i].@id.toString()) == -1){
					str += "ERROR: \""+_xml..Pairing[i].toXMLString() +"\", person's id not present in the People list.\n";	
					_error = true;
				}
			}
			
			if(_error == false){
				str += "All PeoplePairings in Solve are for people included in this case.\n"
			}
			
			_error = false;
			str += "\n";
			str += _xml..Question.length() + " questions in this case.\n";
			str += "\n";
			substr = "";
			_errorCount = 0;
			for(i=0;i<_xml..Question.length();i++){
				if(_xml..Question[i].text() == ""){
					substr += _xml..Question[i].@id + "\n";
					_error = true;
					_errorCount++;
				}
			}
			
			if(_error == true){
				str += "\nERROR, " + _errorCount + " empty questions: \n";
				str += substr;
			} else {
				
			}
			substr = ""
			_errorCount = 0;
			for (i=0;i<_xml..Question.length();i++){
				questionIDList.push(_xml..Question[i].@id);
			}
			for(i=0;i<_xml..RelatedQuestion.length();i++){
				if(questionIDList.indexOf(_xml..RelatedQuestion[i].@id.toString()) == -1){
					substr += "\""+_xml..RelatedQuestion[i].toXMLString()  +"\"\n";// , points to a non-existent question.\n";
					_error = true;
					_errorCount++;
				}
			}
			
			
			if (_error == false){
				str += "All \"RelatedQuestions\" are present in this case study.";
			} else {
				str += "\nERROR, " + _errorCount +" RelatedQuestions point to non-existent questions: \n" + substr;
			}
			
			_TextTarget.text = str;
			
		}
	}
}